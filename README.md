# README #

This repo if for an assignment for INFO2300 at Conestoga College

### What is this repository for? ###

* This repo is for viewing and maintaining my personal portfolio.

### How do I get set up? ###

* Clone repo to your local computer (or download the files to your local computer).
* Using your system's directory navigation, navigate to where the files were saved. 
* Open the file called index.html using an internet browser of your choice. 
* From here you will be able to see my portfolio page.

### Who do I talk to? ###

* For tech support please contact nsaccon9163@conestogac.on.ca

Note: The lisence for this repo is an MIT license. This is so anyone can create a similar portfolio for your own work. 